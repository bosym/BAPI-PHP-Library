# BAPI-PHP-Library
## Features Completed

**Main API Endpoints:**
- [x] /
- [x] /message
- [x] /status
- [x] /status/position

## Requirements
PHP 7.1 or higher.

BAPI Client 1.0.1 or higher.
## Basic Example

```php
$options = [
	'accesstoken' => 'ACCESTOKEN',
	'url' => 'DOMAIN',
];
$bapi = new \BAPI\BAPI($options);

// Get responses as JSON, default false
$bapi->setReturnJson(true);

// Send a message to the player
$bapi->sendMessageToPlayer("Test");

// Get the y position of the player
echo $bapi->getPosition()->getResponse()["y"];
```

See the [example.php](example.php) directory for more common use cases.
## Installation

Either pull in the library via composer:

```bash
$ composer require bosym/bapi-php-library

```

or add the following dependency to your `composer.json` file and run `composer install`:

```json
"bosym/bapi-php-library": "0.1.*"
```
