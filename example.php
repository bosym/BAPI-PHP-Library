<?php
	require __DIR__ . '\vendor\autoload.php';
	use BAPI\BAPI;
	use BAPI\Utils\ColorCodes;

	// Init
	$options = [
		'accesstoken' => 'Key1',
		'url' => 'http://localhost:5250/',
	];
	$bapi = new BAPI($options);

	$message = ColorCodes::AQUA . 'Test von BAPI PHP Library!';

	$bapi->setReturnJson(true);

	var_dump($bapi->isReachable());
	echo '<br>';
	var_dump($bapi->sendMessageToPlayer($message)->getResponse());
	echo '<br>';
	#var_dump($bapi->sendMessageFromPlayer($message)->getResponse());
	echo '<br>';
	var_dump($bapi->getPosition()->getResponse());
	echo '<br>';
	var_dump($bapi->getStatus()->getResponse());


?>