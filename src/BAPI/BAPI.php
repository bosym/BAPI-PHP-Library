<?php

namespace BAPI;

use BAPI\Exceptions\AccessTokenRequiredException;
use BAPI\Exceptions\NotReachableException;
use BAPI\Exceptions\URLRequiredException;
use BAPI\Endpoint\Message;
use BAPI\Endpoint\StatusPosition;
use BAPI\Endpoint\Status;
use BAPI\Endpoint\Base;

class BAPI {
	use Message;
	use StatusPosition;
	use Status;
	use Base;
	/**
	 * @var String
	 */
	public static $token;
	/**
	 * @var String
	 */
	public static $url;
	/**
	 * @var boolean
	 */
	private $returnAsJson;
	private $response;

	public function __construct(Array $options) {

		if (!isset($options['accesstoken']))
			throw new AccessTokenRequiredException();
		if (!isset($options['url']))
			throw new URLRequiredException();

		BAPI::$token = $options['accesstoken'];
		BAPI::$url = $options['url'];
		$this->returnAsJson = (isset($options["returnAsJson"]) ? $options["returnAsJson"] : false);

		if (!$this->isReachable())
			throw new NotReachableException();
	}

	/**
	 * @param boolean $bool
	 */
	public function setReturnJson($bool) {
		$this->returnAsJson = $bool;
	}

	/**
	 * @return bool
	 */
	public function isReachable() : bool {
		$request = new BAPIRequest(BAPI::$url, array('http'=>array('timeout' => 2)));
		$response = $request->sendGet();
		if (empty($response))
			return false;
		else
			return true;
	}

	/**
	 * @return array|string
	 */
	public function getResponse() {
		if ($this->returnAsJson)
			return $this->response;
		else
			return json_decode($this->response, true);
	}
}