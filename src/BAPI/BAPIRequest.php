<?php

namespace BAPI;

use BAPI\Utils\Strings;

class BAPIRequest {

	/**
	 * @var String
	 */
	private $url;
	private $ctx;
	private $uri = [];

	public function __construct(String $base, $ctx = array()) {
		$this->url = $base;
		$this->ctx = stream_context_create($ctx);
	}

	/**
	 * @param String $key
	 * @param $value
	 *
	 * @return BAPIRequest
	 */
	public function addUri(String $key, $value) : BAPIRequest {
		$this->uri[] = $key . '=' . rawurlencode ($value);

		return $this;
	}

	/**
	 * @param String $endpoint
	 *
	 * @return \BAPI\BAPIRequest
	 */
	public function addEndpoint(String $endpoint) : BAPIRequest {
		if (Strings::endsWith($this->url, '/'))
			$this->url = $this->url . $endpoint;
		else
			$this->url = $this->url . '/' . $endpoint;

		return $this;
	}

	/**
	 * @return \BAPI\BAPIRequest
	 */
	public function addToken() : BAPIRequest {
		$this->uri[] = 'token=' . BAPI::$token;
		return $this;
	}

	/**
	 * @return \BAPI\BAPIRequest
	 */
	public function finish() : BAPIRequest {
		$uri = implode('&',$this->uri);
		if (strpos($this->url, '?') === true)
			$this->url = $this->url . '&' . $uri;
		else
			$this->url = $this->url . '?' . $uri;

		return $this;
	}

	/**
	 * @return String
	 */
	public function toString() : String {

		return $this->url;
	}

	/**
	 * @return String
	 */
	public function sendGet() : String {
		//$curl = curl_init($this->url);
		//$result = curl_exec($curl);

		@$result = file_get_contents($this->url);

		return $result;
	}
}