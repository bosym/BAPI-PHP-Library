<?php

namespace BAPI\Endpoint;

use BAPI\BAPI;
use BAPI\BAPIRequest;

trait Base {
	/**
	 * @return \BAPI\BAPI|\BAPI\Endpoint\Base
	 */
	public function getCredits() {
		$request = new BAPIRequest(BAPI::$url);
		$response = $request
			->finish()
			->sendGet();

		$this->response = $response;

		return $this;

	}
}