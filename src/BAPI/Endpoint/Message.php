<?php

namespace BAPI\Endpoint;

use BAPI\BAPI;
use BAPI\BAPIRequest;

trait Message {

	/**
	 * Endpoint /message
	 *
     * @param String $receiver
	 * @param String $message
	 *
	 * @return \BAPI\BAPI|\BAPI\Endpoint\Message
	 */
	public function sendMessage(String $receiver, String $message) {
		$request = new BAPIRequest(BAPI::$url);
		$response = $request
			->addEndpoint("message")
			->addToken()
			->addUri("reciever", $receiver)
			->addUri("message", $message)
			->finish()
			->sendGet();

		$this->response = $response;

		return $this;
	}

	public function sendMessageToPlayer(String $message) {
		return $this->sendMessage('client', $message);
	}

	public function sendMessageFromPlayer(String $message) {
		return $this->sendMessage('server', $message);

	}
}