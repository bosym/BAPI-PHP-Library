<?php

namespace BAPI\Endpoint;

use BAPI\BAPI;
use BAPI\BAPIRequest;

trait Status {
	/**
	 * @return \BAPI\BAPI|\BAPI\Endpoint\Status
	 */
	public function getStatus() {
		$request = new BAPIRequest(BAPI::$url);
		$response = $request
			->addEndpoint("status")
			->addToken()
			->finish()
			->sendGet();

		$this->response = $response;

		return $this;

	}
}