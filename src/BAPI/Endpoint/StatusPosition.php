<?php

namespace BAPI\Endpoint;

use BAPI\BAPI;
use BAPI\BAPIRequest;

trait StatusPosition {
	/**
	 * @return \BAPI\BAPI|\BAPI\Endpoint\StatusPosition
	 */
	public function getPosition() {
		$request = new BAPIRequest(BAPI::$url);
		$response = $request
			->addEndpoint("status")
			->addEndpoint("position")
			->addToken()
			->finish()
			->sendGet();

		$this->response = $response;

		return $this;
	}
}