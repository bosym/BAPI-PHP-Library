<?php

namespace BAPI\Exceptions;

class AccessTokenRequiredException extends BAPIException {

	public function __construct()
	{
		parent::__construct('You must provide a \'accesstoken\' option.');
	}
}