<?php

namespace bapi\Exceptions;

class BAPIException extends \Exception {
	/**
	 * @var string $message
	 * @var int    $code
	 */
	public function __construct($message, $code = 0)
	{
		parent::__construct($message, $code);
	}
}