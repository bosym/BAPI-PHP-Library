<?php

namespace BAPI\Exceptions;

class NotReachableException extends BAPIException {

	public function __construct()
	{
		parent::__construct('Client not reachable.');
	}
}