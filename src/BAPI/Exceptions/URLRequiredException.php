<?php

namespace BAPI\Exceptions;

class URLRequiredException extends BAPIException  {

	public function __construct()
	{
		parent::__construct('You must provide a \'url\' option.');
	}
}