<?php

namespace BAPI\Utils;

abstract class ColorCodes {
	const BLACK = '§0';
	const DARK_BLUE = '§1';
	const DARK_GREEN = '§2';
	const DARK_AQUA = '§3';
	const DARK_RED= '§4';
	const DARK_PURPLE= '§5';
	const GOLD = '§6';
	const GRAY = '§7';
	const DARK_GRAY = '§8';
	const BLUE = '§9';
	const GREEN = '§a';
	const AQUA = '§b';
	const RED = '§c';
	const LIGHT_PURPLE = '§d';
	const YELLOW = '§e';
	const WHITE = '§f';

	const BOLD = '§l';
	const UNDERLINE = '§n';
	const ITALIC = '§o';
	const STRIKE = '§m';
	const MAGIC = '§k';
	const RESET = '§r';
}